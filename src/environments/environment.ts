// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAgabcJSxdDDA1iYkIqgMhonalobgKZl0M",
    authDomain: "skydive-d9812.firebaseapp.com",
    databaseURL: "https://skydive-d9812.firebaseio.com",
    projectId: "skydive-d9812",
    storageBucket: "skydive-d9812.appspot.com",
    messagingSenderId: "927387693487"
    // apiKey: 'AIzaSyCnaHcfwHQxhxIoFdVx81zAcwxzNZEOpPg',
    // authDomain: 'testing-34050.firebaseapp.com',
    // databaseURL: 'https://testing-34050.firebaseio.com',
    // projectId: 'testing-34050',
    // storageBucket: 'testing-34050.appspot.com',
    // messagingSenderId: '380622482344',
  },
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
