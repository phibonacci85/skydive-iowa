import { Component, OnInit } from '@angular/core';
import { User } from '../models';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { RootStoreState, AuthStoreSelectors } from '../root-store';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  user$: Observable<User>;

  constructor(
    private store: Store<RootStoreState.State>,
  ) { }

  ngOnInit() {
    this.user$ = this.store.select(AuthStoreSelectors.selectAuthUser);
  }

}
