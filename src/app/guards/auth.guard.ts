import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { RootStoreState, AuthStoreSelectors } from '../root-store';
import { User } from '../models';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {

  constructor(
    private store: Store<RootStoreState.State>,
    private router: Router,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.select(AuthStoreSelectors.selectAuthUser).pipe(
      take(1),
      map((user: User) => {
        if (!user) {
          this.router.navigate(['/login']);
        }
        return user != null;
      })
    );
  }
}
