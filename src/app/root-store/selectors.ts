import { createSelector, MemoizedSelector } from '@ngrx/store';
import { AuthStoreSelectors } from './auth-store';
import { UiStoreSelectors } from './ui-store';
import { QueueStoreSelectors } from './queue-store';
import { LoadStoreSelectors } from './load-store';
import { JumpStoreSelectors } from './jump-store';
import { JumperStoreSelectors } from './jumper-store';
import { UsersStoreSelectors } from './users-store';

export const selectError: MemoizedSelector<object, string> = createSelector(
  AuthStoreSelectors.selectAuthError,
  UiStoreSelectors.selectUiError,
  QueueStoreSelectors.selectQueueError,
  LoadStoreSelectors.selectLoadError,
  JumpStoreSelectors.selectJumpError,
  JumperStoreSelectors.selectJumperError,
  UsersStoreSelectors.selectUsersError,
  (authError: any, uiError: any, queueError: any, loadError: any, jumpError: any, jumperError: any, usersError: any) => {
    return authError || uiError || queueError || loadError || jumpError || jumperError || usersError;
  },
);

export const selectIsLoading: MemoizedSelector<object, boolean> = createSelector(
  AuthStoreSelectors.selectAuthIsLoading,
  UiStoreSelectors.selectUiIsLoading,
  QueueStoreSelectors.selectQueueIsLoading,
  LoadStoreSelectors.selectLoadIsLoading,
  JumpStoreSelectors.selectJumpIsLoading,
  JumperStoreSelectors.selectJumperIsLoading,
  UsersStoreSelectors.selectUsersIsLoading,
  (auth: boolean, ui: boolean, queue: boolean, load: boolean, jump: boolean, jumper: boolean, users: boolean) => {
    return auth || ui || queue || load || jump || jumper || users;
  },
);
