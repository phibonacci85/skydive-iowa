import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AuthStoreModule } from './auth-store';
import { UiStoreModule } from './ui-store';
import { QueueStoreModule } from './queue-store';
import { LoadStoreModule } from './load-store';
import { JumpStoreModule } from './jump-store';
import { UsersStoreModule } from './users-store';
import { JumperStoreModule } from './jumper-store';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({maxAge: 25}),
    AuthStoreModule,
    UiStoreModule,
    QueueStoreModule,
    LoadStoreModule,
    JumpStoreModule,
    UsersStoreModule,
    JumperStoreModule,
  ],
  declarations: [],
})
export class RootStoreModule {}
