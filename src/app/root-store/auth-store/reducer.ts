import { Actions, ActionTypes } from './actions';
import { initialState, State } from './state';

export function authReducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.AUTHENTICATE:
      return {
        ...state,
        isLoading: true,
      };
    case ActionTypes.NOT_AUTHENTICATED:
      return {
        ...state,
        user: null,
        isLoading: false,
      };
    case ActionTypes.AUTHENTICATED:
      return {
        ...state,
        user: action.payload,
        isLoading: false,
      };
    case ActionTypes.REGISTER:
      return {
        ...state,
        isLoading: true,
      };
    case ActionTypes.LOGIN:
      return {
        ...state,
        isLoading: true,
      };
    case ActionTypes.LOGOUT:
      return {
        ...state,
        isLoading: true,
      };
    case ActionTypes.ERROR:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    default:
      return state;
  }
}