import { User } from '../../models';

export interface State {
  user: User | null;
  isLoading: boolean;
  error: any;
}

export const initialState: State = {
  user: null,
  isLoading: false,
  error: null,
};
