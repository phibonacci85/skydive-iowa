import { Action } from '@ngrx/store';
import { User } from '../../models';

export enum ActionTypes {
 AUTHENTICATE = '[Auth] Authenticate',
 NOT_AUTHENTICATED = '[Auth] Not Authenticated',
 FETCH_USER = '[Auth] Fetch User',
 AUTHENTICATED = '[Auth] Authenticated',
 REGISTER = '[Auth] Register',
 LOGIN = '[Auth] Login',
 GOOGLE_REGISTER = '[Auth] Google Register',
 GOOGLE_LOGIN = '[Auth] Google Login',
 LOGOUT = '[Auth] Logout',
 ERROR = '[Auth] Error',
}

export class Authenticate implements Action {
  readonly type = ActionTypes.AUTHENTICATE;

  constructor(public payload?: any) {}
}

export class NotAuthenticated implements Action {
  readonly type = ActionTypes.NOT_AUTHENTICATED;

  constructor(public payload?: any) {}
}

export class FetchUser implements Action {
  readonly type = ActionTypes.FETCH_USER;

  constructor(public payload: string) {}
}

export class Authenticated implements Action {
  readonly type = ActionTypes.AUTHENTICATED;

  constructor(public payload: User) {}
}

export class Register implements Action {
  readonly type = ActionTypes.REGISTER;

  constructor(public payload: { username: string, password: string }) {}
}

export class Login implements Action {
  readonly type = ActionTypes.LOGIN;

  constructor(public payload: { username: string, password: string }) {}
}

export class GoogleRegister implements Action {
  readonly type = ActionTypes.GOOGLE_REGISTER;

  constructor(public payload?: any) {}
}

export class GoogleLogin implements Action {
  readonly type = ActionTypes.GOOGLE_LOGIN;

  constructor(public payload?: any) {}
}

export class Logout implements Action {
  readonly type = ActionTypes.LOGOUT;

  constructor(public payload?: any) {}
}

export class Error implements Action {
  readonly type = ActionTypes.ERROR;

  constructor(public payload: any) {}
}

export type Actions =
  Authenticate
  | NotAuthenticated
  | FetchUser
  | Authenticated
  | Register
  | Login
  | GoogleRegister
  | GoogleLogin
  | Logout
  | Error;
