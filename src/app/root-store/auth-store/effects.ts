import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { from, of } from 'rxjs/index';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { MatSnackBar } from '@angular/material';
import { AngularFirestore } from 'angularfire2/firestore';

import { User } from '../../models';
import * as authActions from './actions';
import * as firebase from 'firebase';

@Injectable()
export class AuthStoreEffects {

  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private actions$: Actions,
    private snackBar: MatSnackBar,
    private router: Router,
  ) {}

  @Effect()
  authenticate$ = this.actions$.ofType(authActions.ActionTypes.AUTHENTICATE)
    .pipe(
      map((action: authActions.Authenticate) => action.payload),
      switchMap(() => of(this.afAuth.auth)),
      tap(auth => {
        // console.log('login:' + auth.currentUser.uid);
        return auth;
      }),
      map(auth => {
        if (auth.currentUser) {
          return new authActions.FetchUser(auth.currentUser.uid);
        } else {
          return new authActions.NotAuthenticated();
        }
      }),
      catchError(err => {
        console.log(err);
        return of(new authActions.Error(err));
      }),
    );

  @Effect()
  fetchUser$ = this.actions$.ofType(authActions.ActionTypes.FETCH_USER).pipe(
    map((action: authActions.FetchUser) => action.payload),
    switchMap(
      (uid: string) => this.afs.doc<User>(`users/${uid}`).valueChanges()),
    map((currentUser: User) => {
      this.router.navigate(['/schedule']);
      return new authActions.Authenticated(currentUser);
    }),
    catchError(err => of(new authActions.Error(err))),
  );

  @Effect()
  register$ = this.actions$.ofType(authActions.ActionTypes.REGISTER).pipe(
    map((action: authActions.Register) => action.payload),
    switchMap((authData: { username: string, password: string }) => from(
      this.afAuth.auth.createUserWithEmailAndPassword(
        authData.username,
        authData.password,
      ))
      .pipe(
        switchMap(credential => {
          console.log(credential);
          return this.afs.doc(`users/${credential.user.uid}`).set({
            uid: credential.user.uid,
            displayName: 'Something',
            photoURL: '',
            email: '',
            roles: {
              admin: false,
              jumper: false,
              manager: false,
              user: true,
            },
          });
        }),
        map(credential => new authActions.Authenticate()),
        catchError(err => of(new authActions.Error(err))),
      ),
    ),
    catchError(err => of(new authActions.Error(err))),
  );

  @Effect()
  login$ = this.actions$.ofType(authActions.ActionTypes.LOGIN).pipe(
    map((action: authActions.Login) => action.payload),
    switchMap((authData: { username: string, password: string }) => from(
      this.afAuth.auth.signInWithEmailAndPassword(
        authData.username,
        authData.password,
      ))
      .pipe(
        map(credential => new authActions.Authenticate()),
        catchError(err => of(new authActions.Error(err))),
      ),
    ),
    catchError(err => of(new authActions.Error(err))),
  );

  @Effect()
  googleRegister$ = this.actions$.ofType(
    authActions.ActionTypes.GOOGLE_REGISTER).pipe(
    map((action: authActions.GoogleLogin) => action.payload),
    switchMap(() => from(this.googleLogin())
      .pipe(
        switchMap(credential => {
          console.log(credential);
          return this.afs.doc(`users/${credential.user.uid}`).set({
            uid: credential.user.uid,
            displayName: credential.user.displayName,
            photoURL: credential.user.photoURL,
            email: credential.user.email,
            roles: {
              admin: false,
              jumper: false,
              manager: false,
              user: true,
            },
          });
        }),
        map(credential => new authActions.Authenticate()),
        catchError(err => of(new authActions.Error(err))),
      ),
    ),
    catchError(err => of(new authActions.Error(err))),
  );

  @Effect()
  googleLogin$ = this.actions$.ofType(authActions.ActionTypes.GOOGLE_LOGIN)
    .pipe(
      map((action: authActions.GoogleLogin) => action.payload),
      switchMap(() => from(this.googleLogin())
        .pipe(
          map(credential => new authActions.Authenticate()),
          catchError(err => of(new authActions.Error(err))),
        ),
      ),
      catchError(err => of(new authActions.Error(err))),
    );

  @Effect()
  logout$ = this.actions$.ofType(authActions.ActionTypes.LOGOUT).pipe(
    map((action: authActions.Logout) => action.payload),
    switchMap(payload => of(this.afAuth.auth.signOut())),
    map(() => {
      this.router.navigate(['/login']);
      return new authActions.NotAuthenticated();
    }),
    catchError(err => of(new authActions.Error(err))),
  );

  @Effect({dispatch: false})
  error$ = this.actions$.ofType(authActions.ActionTypes.ERROR).pipe(
    map((action: authActions.Error) => action.payload),
    tap(payload => this.snackBar.open(payload.message, null, {duration: 2000})),
  );

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.afAuth.auth.signInWithPopup(provider);
  }
}
