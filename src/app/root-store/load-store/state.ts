import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Load } from '../../models';

export const featureAdapter: EntityAdapter<Load> = createEntityAdapter<Load>({
  selectId: model => model.id,
  sortComparer: (a: Load, b: Load): number =>
    b.id.toString().localeCompare(a.id.toString()),
});

export interface State extends EntityState<Load> {
  isLoading?: boolean;
  error?: any;
}

export const initialState: State = featureAdapter.getInitialState(
  {
    isLoading: false,
    error: null,
  },
);
