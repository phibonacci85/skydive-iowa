import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, from, of } from 'rxjs';
import { map, switchMap, flatMap } from 'rxjs/operators';

import { Load } from '../../models';
import * as loadActions from './actions';
import 'rxjs-compat/add/operator/map';

@Injectable()
export class LoadStoreEffects {
  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private actions$: Actions,
    private snackBar: MatSnackBar,
    private router: Router) {}

  @Effect()
  loadRequestEffect$: Observable<Action> = this.actions$.pipe(
    ofType(loadActions.ActionTypes.LOAD_REQUEST),
    map((action: loadActions.LoadRequestAction) => action.payload),
    switchMap(() => {
      const ref = this.afs.collection<Load>('loads');
      return ref.snapshotChanges().map(arr => {
        return arr.map(doc => {
          const data = doc.payload.doc.data();
          return {
            id: doc.payload.doc.id,
            queueId: data.queueId,
          } as Load;
        });
      });
    }),
    map((loads: Load[]) => {
      return new loadActions.LoadSuccessAction({items: loads});
    }),
  );

  @Effect()
  createLoad$: Observable<Action> = this.actions$.pipe(
    ofType(loadActions.ActionTypes.CREATE_LOAD),
    map((action: loadActions.CreateLoadAction) => action.payload),
    switchMap(data => {
      const ref = this.afs.collection('loads');
      return from(ref.add({queueId: data.queueId}));
    }),
    map(docRef => {
      return new loadActions.SuccessAction();
    }),
  );

  @Effect()
  deleteLoad$: Observable<Action> = this.actions$.pipe(
    ofType(loadActions.ActionTypes.DELETE_LOAD),
    map((action: loadActions.DeleteLoadAction) => action.payload),
    switchMap(data => {
      const ref = this.afs.doc(`loads/${data.loadId}`);
      return from(ref.delete());
    }),
    map(() => {
      return new loadActions.SuccessAction();
    }),
  );
}
