import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LoadStoreEffects } from './effects';
import { loadReducer } from './reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('load', loadReducer),
    EffectsModule.forFeature([LoadStoreEffects])
  ],
  providers: [LoadStoreEffects]
})
export class LoadStoreModule { }
