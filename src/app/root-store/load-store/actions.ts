import { Action } from '@ngrx/store';
import { Load } from '../../models';

export enum ActionTypes {
  LOAD_REQUEST = '[Load] Load Request',
  LOAD_FAILURE = '[Load] Load Failure',
  LOAD_SUCCESS = '[Load] Load Success',
  CREATE_LOAD = '[Load] Create Load',
  DELETE_LOAD = '[Load] Delete Load',
  SUCCESS = '[Load] Success',
}

export class LoadRequestAction implements Action {
  readonly type = ActionTypes.LOAD_REQUEST;

  constructor(public payload?: any) {}
}

export class LoadFailureAction implements Action {
  readonly type = ActionTypes.LOAD_FAILURE;

  constructor(public payload: { error: string }) {}
}

export class LoadSuccessAction implements Action {
  readonly type = ActionTypes.LOAD_SUCCESS;

  constructor(public payload: { items: Load[] }) {}
}

export class CreateLoadAction implements Action {
  readonly type = ActionTypes.CREATE_LOAD;

  constructor(public payload: { queueId: string }) {}
}

export class DeleteLoadAction implements Action {
  readonly type = ActionTypes.DELETE_LOAD;

  constructor(public payload: { loadId: string }) {}
}

export class SuccessAction implements Action {
  readonly type = ActionTypes.SUCCESS;

  constructor(public payload?: any) {}
}

export type Actions =
  LoadRequestAction
  | LoadFailureAction
  | LoadSuccessAction
  | CreateLoadAction
  | DeleteLoadAction
  | SuccessAction;
