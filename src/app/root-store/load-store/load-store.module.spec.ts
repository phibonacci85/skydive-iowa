import { LoadStoreModule } from './load-store.module';

describe('LoadStoreModule', () => {
  let loadStoreModule: LoadStoreModule;

  beforeEach(() => {
    loadStoreModule = new LoadStoreModule();
  });

  it('should create an instance', () => {
    expect(loadStoreModule).toBeTruthy();
  });
});
