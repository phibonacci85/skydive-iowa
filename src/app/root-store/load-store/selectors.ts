import {
  createFeatureSelector,
  createSelector,
  MemoizedSelector,
} from '@ngrx/store';

import { Load } from '../../models';

import { featureAdapter, State } from './state';

export const getError = (state: State): any => state.error;

export const getIsLoading = (state: State): boolean => state.isLoading;

export const selectLoadState: MemoizedSelector<object, State> = createFeatureSelector<State>(
  'load',
);

export const selectAllLoadItems: (state: object) => Load[] = featureAdapter.getSelectors(
  selectLoadState,
).selectAll;

export const selectLoadById = (id: string) => createSelector(
  selectAllLoadItems, (allLoads: Load[]) => {
    if (allLoads) {
      return allLoads.find(p => p.id === id);
    } else {
      return null;
    }
  },
);

export const selectLoadsByIds = (ids: string[]) => createSelector(
  selectAllLoadItems, (allLoads: Load[]) => {
    if (allLoads) {
      return allLoads.filter(l => ids.includes(l.id));
    } else {
      return null;
    }
  },
);

export const selectLoadsByQueueId = (queueId: string) => createSelector(
  selectAllLoadItems, (allLoads: Load[]) => {
    if (allLoads) {
      return allLoads.filter(l => l.queueId === queueId);
    } else {
      return null;
    }
  },
);

export const selectLoadError: MemoizedSelector<object, any> = createSelector(
  selectLoadState,
  getError,
);

export const selectLoadIsLoading: MemoizedSelector<object,
  boolean> = createSelector(selectLoadState, getIsLoading);
