import { Actions, ActionTypes } from './actions';
import { featureAdapter, initialState, State } from './state';

export function loadReducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.LOAD_REQUEST: {
      return {
        ...state,
        isLoading: true,
        error: null
      };
    }
    case ActionTypes.LOAD_SUCCESS: {
      return featureAdapter.addAll(action.payload.items, {
        ...state,
        isLoading: false,
        error: null
      });
    }
    case ActionTypes.LOAD_FAILURE: {
      return {
        ...state,
        isLoading: false,
        error: action.payload.error
      };
    }
    case ActionTypes.CREATE_LOAD: {
      return {
        ...state,
        isLoading: true,
        error: null
      };
    }
    case ActionTypes.DELETE_LOAD: {
      return {
        ...state,
        isLoading: true,
        error: null
      };
    }
    case ActionTypes.SUCCESS: {
      return {
        ...state,
        isLoading: false,
        error: null
      };
    }
    default: {
      return state;
    }
  }
}