import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Queue } from '../../models';

export const featureAdapter: EntityAdapter<Queue> = createEntityAdapter<Queue>({
  selectId: model => model.id,
  sortComparer: (a: Queue, b: Queue): number =>
    b.id.toString().localeCompare(a.id.toString()),
});

export interface State extends EntityState<Queue> {
  isLoading?: boolean;
  error?: any;
}

export const initialState: State = featureAdapter.getInitialState(
  {
    isLoading: false,
    error: null,
  },
);
