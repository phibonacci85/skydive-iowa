import { Action } from '@ngrx/store';
import { Queue } from '../../models';

export enum ActionTypes {
  LOAD_REQUEST = '[Queue] Load Request',
  LOAD_FAILURE = '[Queue] Load Failure',
  LOAD_SUCCESS = '[Queue] Load Success',
}

export class LoadRequestAction implements Action {
  readonly type = ActionTypes.LOAD_REQUEST;

  constructor(public payload?: any) {}
}

export class LoadFailureAction implements Action {
  readonly type = ActionTypes.LOAD_FAILURE;

  constructor(public payload: { error: string }) {}
}

export class LoadSuccessAction implements Action {
  readonly type = ActionTypes.LOAD_SUCCESS;

  constructor(public payload: { items: Queue[] }) {}
}

export type Actions =
  LoadRequestAction
  | LoadFailureAction
  | LoadSuccessAction;
