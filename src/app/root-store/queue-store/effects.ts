import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { Queue } from '../../models';
import * as queueActions from './actions';
import 'rxjs-compat/add/operator/map';

@Injectable()
export class QueueStoreEffects {
  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private actions$: Actions,
    private snackBar: MatSnackBar,
    private router: Router) {}

  @Effect()
  loadRequestEffect$: Observable<Action> = this.actions$.pipe(
    ofType(queueActions.ActionTypes.LOAD_REQUEST),
    map((action: queueActions.LoadRequestAction) => action.payload),
    switchMap(() => {
      const ref = this.afs.collection<Queue>('queues');
      return ref.snapshotChanges().map(arr => {
        return arr.map(doc => {
          const data = doc.payload.doc.data();
          return {
            id: doc.payload.doc.id,
            aircraft: data.aircraft,
          } as Queue;
        });
      });
    }),
    map((queues: Queue[]) => {
      return new queueActions.LoadSuccessAction({items: queues});
    }),
  );
}
