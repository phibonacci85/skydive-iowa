import {
  createFeatureSelector,
  createSelector,
  MemoizedSelector,
} from '@ngrx/store';

import { Queue } from '../../models';

import { featureAdapter, State } from './state';

export const getError = (state: State): any => state.error;

export const getIsLoading = (state: State): boolean => state.isLoading;

export const selectQueueState: MemoizedSelector<object, State> = createFeatureSelector<State>(
  'queue',
);

export const selectAllQueueItems: (
  state: object
) => Queue[] = featureAdapter.getSelectors(selectQueueState).selectAll;

export const selectQueueById = (id: string) => createSelector(
  selectAllQueueItems, (allQueues: Queue[]) => {
    if (allQueues) {
      return allQueues.find(p => p.id === id);
    } else {
      return null;
    }
  },
);

export const selectQueueError: MemoizedSelector<object, any> = createSelector(
  selectQueueState,
  getError,
);

export const selectQueueIsLoading: MemoizedSelector<object,
  boolean> = createSelector(selectQueueState, getIsLoading);
