import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { QueueStoreEffects } from './effects';
import { queueReducer } from './reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('queue', queueReducer),
    EffectsModule.forFeature([QueueStoreEffects]),
  ],
  providers: [QueueStoreEffects],
})
export class QueueStoreModule {}
