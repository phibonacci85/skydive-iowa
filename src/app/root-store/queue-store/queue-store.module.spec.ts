import { QueueStoreModule } from './queue-store.module';

describe('QueueStoreModule', () => {
  let queueStoreModule: QueueStoreModule;

  beforeEach(() => {
    queueStoreModule = new QueueStoreModule();
  });

  it('should create an instance', () => {
    expect(queueStoreModule).toBeTruthy();
  });
});
