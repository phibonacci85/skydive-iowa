import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { Jumper } from '../../models';
import * as jumperActions from './actions';
import 'rxjs-compat/add/operator/map';
import { from } from 'rxjs';

@Injectable()
export class JumperStoreEffects {
  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private actions$: Actions,
    private snackBar: MatSnackBar,
    private router: Router) {}

  @Effect()
  loadRequestEffect$: Observable<Action> = this.actions$.pipe(
    ofType(jumperActions.ActionTypes.LOAD_REQUEST),
    map((action: jumperActions.LoadRequestAction) => action.payload),
    switchMap(() => {
      const ref = this.afs.collection<Jumper>('jumpers');
      return ref.snapshotChanges().map(arr => {
        return arr.map(doc => {
          const data = doc.payload.doc.data();
          return {
            id: doc.payload.doc.id,
            role: data.role,
            uid: data.uid,
            jumpId: data.jumpId
          } as Jumper;
        });
      });
    }),
    map((jumpers: Jumper[]) => {
      return new jumperActions.LoadSuccessAction({items: jumpers});
    }),
  );

  @Effect()
  createJumper$: Observable<Action> = this.actions$.pipe(
    ofType(jumperActions.ActionTypes.CREATE_JUMPER),
    map((action: jumperActions.CreateJumperAction) => action.payload),
    switchMap(data => {
      const ref = this.afs.collection('jumpers');
      return from(ref.add({role: data.role, uid: data.uid, jumpId: data.jumpId}));
    }),
    map(docRef => {
      return new jumperActions.SuccessAction();
    }),
  );

  @Effect()
  deleteJumper$: Observable<Action> = this.actions$.pipe(
    ofType(jumperActions.ActionTypes.DELETE_JUMPER),
    map((action: jumperActions.DeleteJumperAction) => action.payload),
    switchMap(data => {
      const ref = this.afs.doc(`jumpers/${data.jumperId}`);
      return from(ref.delete());
    }),
    map(() => {
      return new jumperActions.SuccessAction();
    }),
  );
}
