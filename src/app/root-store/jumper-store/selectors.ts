import {
  createFeatureSelector,
  createSelector,
  MemoizedSelector,
} from '@ngrx/store';

import { Jumper } from '../../models';

import { featureAdapter, State } from './state';

export const getError = (state: State): any => state.error;

export const getIsLoading = (state: State): boolean => state.isLoading;

export const selectJumperState: MemoizedSelector<object, State> = createFeatureSelector<State>(
  'jumper',
);

export const selectAllJumperItems: (state: object) => Jumper[] = featureAdapter.getSelectors(
  selectJumperState,
).selectAll;

export const selectJumperById = (id: string) => createSelector(
  selectAllJumperItems, (allJumps: Jumper[]) => {
    if (allJumps) {
      return allJumps.find(p => p.id === id);
    } else {
      return null;
    }
  },
);

export const selectJumpersByIds = (ids: string[]) => createSelector(
  selectAllJumperItems, (allJumpers: Jumper[]) => {
    if (allJumpers) {
      return allJumpers.filter(j => ids.includes(j.id));
    } else {
      return null;
    }
  },
);

export const selectJumpersByJumpId = (jumpId: string) => createSelector(
  selectAllJumperItems, (allJumps: Jumper[]) => {
    if (allJumps) {
      return allJumps.filter(j => j.jumpId === jumpId);
    } else {
      return null;
    }
  },
);

export const selectJumperError: MemoizedSelector<object, any> = createSelector(
  selectJumperState,
  getError,
);

export const selectJumperIsLoading: MemoizedSelector<object,
  boolean> = createSelector(selectJumperState, getIsLoading);
