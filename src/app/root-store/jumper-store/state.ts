import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Jumper } from '../../models';

export const featureAdapter: EntityAdapter<Jumper> = createEntityAdapter<Jumper>({
  selectId: model => model.id,
  sortComparer: (a: Jumper, b: Jumper): number =>
    b.id.toString().localeCompare(a.id.toString()),
});

export interface State extends EntityState<Jumper> {
  isLoading?: boolean;
  error?: any;
}

export const initialState: State = featureAdapter.getInitialState(
  {
    isLoading: false,
    error: null,
  },
);
