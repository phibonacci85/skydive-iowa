import { Action } from '@ngrx/store';
import { Jumper } from '../../models';

export enum ActionTypes {
  LOAD_REQUEST = '[Jumpers] Load Request',
  LOAD_FAILURE = '[Jumpers] Load Failure',
  LOAD_SUCCESS = '[Jumpers] Load Success',
  CREATE_JUMPER = '[Jumpers] Create Jump',
  DELETE_JUMPER = '[Jumpers] Delete Jump',
  SUCCESS = '[Jumpers] Success',
}

export class LoadRequestAction implements Action {
  readonly type = ActionTypes.LOAD_REQUEST;

  constructor(public payload?: any) {}
}

export class LoadFailureAction implements Action {
  readonly type = ActionTypes.LOAD_FAILURE;

  constructor(public payload: { error: string }) {}
}

export class LoadSuccessAction implements Action {
  readonly type = ActionTypes.LOAD_SUCCESS;

  constructor(public payload: { items: Jumper[] }) {}
}

export class CreateJumperAction implements Action {
  readonly type = ActionTypes.CREATE_JUMPER;

  constructor(public payload: { role: string, uid: string, jumpId: string }) {}
}

export class DeleteJumperAction implements Action {
  readonly type = ActionTypes.DELETE_JUMPER;

  constructor(public payload: { jumperId: string }) {}
}

export class SuccessAction implements Action {
  readonly type = ActionTypes.SUCCESS;

  constructor(public payload?: any) {}
}

export type Actions = LoadRequestAction | LoadFailureAction | LoadSuccessAction | CreateJumperAction | DeleteJumperAction | SuccessAction;
