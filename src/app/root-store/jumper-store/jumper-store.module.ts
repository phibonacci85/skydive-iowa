import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { jumperReducer } from './reducer';
import { EffectsModule } from '@ngrx/effects';
import { JumperStoreEffects } from './effects';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('jumper', jumperReducer),
    EffectsModule.forFeature([JumperStoreEffects]),
  ],
  providers: [JumperStoreEffects],
})
export class JumperStoreModule {}
