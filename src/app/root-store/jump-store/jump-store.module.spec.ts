import { JumpStoreModule } from './jump-store.module';

describe('JumpStoreModule', () => {
  let jumpStoreModule: JumpStoreModule;

  beforeEach(() => {
    jumpStoreModule = new JumpStoreModule();
  });

  it('should create an instance', () => {
    expect(jumpStoreModule).toBeTruthy();
  });
});
