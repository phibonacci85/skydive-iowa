import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { JumpStoreEffects } from './effects';
import { jumpReducer } from './reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('jump', jumpReducer),
    EffectsModule.forFeature([JumpStoreEffects])
  ],
  providers: [JumpStoreEffects]
})
export class JumpStoreModule { }
