import { Action } from '@ngrx/store';
import { Jump, Jumper } from '../../models';

export enum ActionTypes {
  LOAD_REQUEST = '[Jump] Load Request',
  LOAD_FAILURE = '[Jump] Load Failure',
  LOAD_SUCCESS = '[Jump] Load Success',
  CREATE_JUMP = '[Jump] Create Jump',
  DELETE_JUMP = '[Jump] Delete Jump',
  SUCCESS = '[Jump] Success',
}

export class LoadRequestAction implements Action {
  readonly type = ActionTypes.LOAD_REQUEST;

  constructor(public payload?: any) {}
}

export class LoadFailureAction implements Action {
  readonly type = ActionTypes.LOAD_FAILURE;

  constructor(public payload: { error: string }) {}
}

export class LoadSuccessAction implements Action {
  readonly type = ActionTypes.LOAD_SUCCESS;

  constructor(public payload: { items: Jump[] }) {}
}

export class CreateJumpAction implements Action {
  readonly type = ActionTypes.CREATE_JUMP;

  constructor(public payload: { type: string, loadId: string, jumpers: {role: string, uid: string}[] }) {}
}

export class DeleteJumpAction implements Action {
  readonly type = ActionTypes.DELETE_JUMP;

  constructor(public payload: { jumpId: string }) {}
}

export class SuccessAction implements Action {
  readonly type = ActionTypes.SUCCESS;

  constructor(public payload?: any) {}
}

export type Actions = LoadRequestAction | LoadFailureAction | LoadSuccessAction | CreateJumpAction | DeleteJumpAction | SuccessAction;
