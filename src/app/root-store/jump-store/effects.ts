import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { map, switchMap, flatMap } from 'rxjs/operators';

import { Jump } from '../../models';
import * as jumpActions from './actions';
import * as jumperActions from '../jumper-store/actions';
import 'rxjs-compat/add/operator/map';
import { from } from 'rxjs/index';

@Injectable()
export class JumpStoreEffects {
  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private actions$: Actions,
    private snackBar: MatSnackBar,
    private router: Router) {}

  @Effect()
  loadRequestEffect$: Observable<Action> = this.actions$.pipe(
    ofType(jumpActions.ActionTypes.LOAD_REQUEST),
    map((action: jumpActions.LoadRequestAction) => action.payload),
    switchMap(() => {
      const ref = this.afs.collection<Jump>('jumps');
      return ref.snapshotChanges().map(arr => {
        return arr.map(doc => {
          const data = doc.payload.doc.data();
          return {
            id: doc.payload.doc.id,
            type: data.type,
            loadId: data.loadId,
          } as Jump;
        });
      });
    }),
    map((jumps: Jump[]) => {
      return new jumpActions.LoadSuccessAction({items: jumps});
    }),
  );

  @Effect()
  createJump$: Observable<Action> = this.actions$.pipe(
    ofType(jumpActions.ActionTypes.CREATE_JUMP),
    map((action: jumpActions.CreateJumpAction) => action.payload),
    flatMap(data => {
      const ref = this.afs.collection('jumps');
      return from(ref.add({loadId: data.loadId, type: data.type})).pipe(
        flatMap(docRef => {
          return data.jumpers.map(jumper => {
            return {
              role: jumper.role,
              uid: jumper.uid,
              jumpId: docRef.id
            };
          });
        }),
      );
    }),
    map(jumper => {
      console.log(jumper);
      return new jumperActions.CreateJumperAction({role: jumper.role, uid: jumper.uid, jumpId: jumper.jumpId});
    }),
  );

  @Effect()
  deleteJump$: Observable<Action> = this.actions$.pipe(
    ofType(jumpActions.ActionTypes.DELETE_JUMP),
    map((action: jumpActions.DeleteJumpAction) => action.payload),
    switchMap(data => {
      const ref = this.afs.doc(`jumps/${data.jumpId}`);
      return from(ref.delete());
    }),
    map(() => {
      return new jumpActions.SuccessAction();
    }),
  );
}
