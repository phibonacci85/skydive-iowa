import {
  createFeatureSelector,
  createSelector,
  MemoizedSelector,
} from '@ngrx/store';

import { Jump } from '../../models';

import { featureAdapter, State } from './state';

export const getError = (state: State): any => state.error;

export const getIsLoading = (state: State): boolean => state.isLoading;

export const selectJumpState: MemoizedSelector<object, State> = createFeatureSelector<State>(
  'jump',
);

export const selectAllJumpItems: (state: object) => Jump[] = featureAdapter.getSelectors(
  selectJumpState,
).selectAll;

export const selectJumpById = (id: string) => createSelector(
  selectAllJumpItems, (allJumps: Jump[]) => {
    if (allJumps) {
      return allJumps.find(p => p.id === id);
    } else {
      return null;
    }
  },
);

export const selectJumpsByIds = (ids: string[]) => createSelector(
  selectAllJumpItems, (allJumps: Jump[]) => {
    if (allJumps) {
      return allJumps.filter(j => ids.includes(j.id));
    } else {
      return null;
    }
  },
);

export const selectJumpsByLoadId = (loadId: string) => createSelector(
  selectAllJumpItems, (allJumps: Jump[]) => {
    if (allJumps) {
      return allJumps.filter(j => j.loadId === loadId);
    } else {
      return null;
    }
  },
);

export const selectJumpError: MemoizedSelector<object, any> = createSelector(
  selectJumpState,
  getError,
);

export const selectJumpIsLoading: MemoizedSelector<object,
  boolean> = createSelector(selectJumpState, getIsLoading);
