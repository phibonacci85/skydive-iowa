import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Jump } from '../../models';

export const featureAdapter: EntityAdapter<Jump> = createEntityAdapter<Jump>({
  selectId: model => model.id,
  sortComparer: (a: Jump, b: Jump): number =>
    b.id.toString().localeCompare(a.id.toString()),
});

export interface State extends EntityState<Jump> {
  isLoading?: boolean;
  error?: any;
}

export const initialState: State = featureAdapter.getInitialState(
  {
    isLoading: false,
    error: null,
  },
);
