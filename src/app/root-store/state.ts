import { AuthStoreState } from './auth-store';
import { UiStoreState } from './ui-store';
import { QueueStoreState } from './queue-store';
import { LoadStoreState } from './load-store';
import { JumpStoreState } from './jump-store';
import { JumperStoreState } from './jumper-store';
import { UsersStoreState } from './users-store';

export interface State {
  auth: AuthStoreState.State;
  ui: UiStoreState.State;
  queue: QueueStoreState.State;
  load: LoadStoreState.State;
  jump: JumpStoreState.State;
  jumper: JumperStoreState.State;
  users: UsersStoreState.State;
}
