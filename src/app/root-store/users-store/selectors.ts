import {
  createFeatureSelector,
  createSelector,
  MemoizedSelector,
} from '@ngrx/store';

import { User } from '../../models';

import { featureAdapter, State } from './state';

export const getError = (state: State): any => state.error;

export const getIsLoading = (state: State): boolean => state.isLoading;

export const selectUsersState: MemoizedSelector<object, State> = createFeatureSelector<State>(
  'users',
);

export const selectAllUsersItems: (state: object) => User[] = featureAdapter.getSelectors(
  selectUsersState,
).selectAll;

export const selectUserById = (id: string) => createSelector(
  selectAllUsersItems, (allUsers: User[]) => {
    if (allUsers) {
      return allUsers.find(p => p.uid === id);
    } else {
      return null;
    }
  },
);

export const selectUsersByIds = (ids: string[]) => createSelector(
  selectAllUsersItems, (allUsers: User[]) => {
    if (ids == null) {
      return [];
    } else if (allUsers) {
      return allUsers.filter(j => ids.includes(j.uid));
    } else {
      return null;
    }
  },
);

export const selectJumpers = (jumper: boolean) => createSelector(
  selectAllUsersItems, (allUsers: User[]) => {
    if (allUsers) {
      return allUsers.filter(j => j.roles.jumper === jumper);
    } else {
      return null;
    }
  },
);

export const selectUsersError: MemoizedSelector<object, any> = createSelector(
  selectUsersState,
  getError,
);

export const selectUsersIsLoading: MemoizedSelector<object,
  boolean> = createSelector(selectUsersState, getIsLoading);
