import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { UsersStoreEffects } from './effects';
import { usersReducer } from './reducer';
import { StoreModule } from '@ngrx/store';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('users', usersReducer),
    EffectsModule.forFeature([UsersStoreEffects])
  ],
  providers: [UsersStoreEffects]
})
export class UsersStoreModule { }
