import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { User } from '../../models';
import * as userActions from './actions';
import 'rxjs-compat/add/operator/map';

@Injectable()
export class UsersStoreEffects {
  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private actions$: Actions,
    private snackBar: MatSnackBar,
    private router: Router) {}

  @Effect()
  loadRequestEffect$: Observable<Action> = this.actions$.pipe(
    ofType(userActions.ActionTypes.LOAD_REQUEST),
    map((action: userActions.LoadRequestAction) => action.payload),
    switchMap(() => {
      const ref = this.afs.collection<User>('users');
      return ref.snapshotChanges().map(arr => {
        return arr.map(doc => {
          const data = doc.payload.doc.data();
          return {
            uid: doc.payload.doc.id,
            displayName: data.displayName,
            email: data.email,
            photoURL: data.photoURL,
            roles: data.roles,
          } as User;
        });
      });
    }),
    map((jumps: User[]) => {
      return new userActions.LoadSuccessAction({items: jumps});
    }),
  );
}