import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { User } from '../../models';

export const featureAdapter: EntityAdapter<User> = createEntityAdapter<User>({
  selectId: model => model.uid,
  sortComparer: (a: User, b: User): number =>
    b.uid.toString().localeCompare(a.uid.toString()),
});

export interface State extends EntityState<User> {
  isLoading?: boolean;
  error?: any;
}

export const initialState: State = featureAdapter.getInitialState(
  {
    isLoading: false,
    error: null,
  },
);
