import { Action } from '@ngrx/store';
import { User } from '../../models';

export enum ActionTypes {
  LOAD_REQUEST = '[Users] Load Request',
  LOAD_FAILURE = '[Users] Load Failure',
  LOAD_SUCCESS = '[Users] Load Success'
}

export class LoadRequestAction implements Action {
  readonly type = ActionTypes.LOAD_REQUEST;

  constructor(public payload?: any) {}
}

export class LoadFailureAction implements Action {
  readonly type = ActionTypes.LOAD_FAILURE;

  constructor(public payload: { error: string }) {}
}

export class LoadSuccessAction implements Action {
  readonly type = ActionTypes.LOAD_SUCCESS;

  constructor(public payload: { items: User[] }) {}
}

export type Actions = LoadRequestAction | LoadFailureAction | LoadSuccessAction;
