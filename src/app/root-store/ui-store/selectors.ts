import {
  createFeatureSelector,
  createSelector,
  MemoizedSelector,
} from '@ngrx/store';

import { State } from './state';

const getError = (state: State): any => state.error;

const getIsLoading = (state: State): boolean => state.isLoading;

const getIsSidenavOpen = (state: State) => state.isSidenavOpen;

export const selectUiState: MemoizedSelector<object, State> = createFeatureSelector<State>(
  'ui'
);

export const selectUiError: MemoizedSelector<object, any> = createSelector(
  selectUiState, getError,
);

export const selectUiIsLoading: MemoizedSelector<object, boolean> = createSelector(
  selectUiState, getIsLoading
);

export const selectUiIsSidenavOpen: MemoizedSelector<object, boolean> = createSelector(
  selectUiState, getIsSidenavOpen
);
