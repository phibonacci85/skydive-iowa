import { Actions, ActionTypes } from './actions';
import { initialState, State } from './state';

export function uiReducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.SET_SIDENAV_OPEN:
      return {
        ...state,
        isSidenavOpen: true,
      };
    case ActionTypes.SET_SIDENAV_CLOSED:
      return {
        ...state,
        isSidenavOpen: false,
      };
    case ActionTypes.ERROR:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    default:
      return state;
  }
}