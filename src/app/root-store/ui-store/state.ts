export interface State {
  isSidenavOpen: boolean;
  isLoading: boolean;
  error: any;
}

export const initialState: State = {
  isSidenavOpen: false,
  isLoading: false,
  error: null,
};
