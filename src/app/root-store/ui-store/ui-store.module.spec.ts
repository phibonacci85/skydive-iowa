import { UiStoreModule } from './ui-store.module';

describe('UiStoreModule', () => {
  let uiStoreModule: UiStoreModule;

  beforeEach(() => {
    uiStoreModule = new UiStoreModule();
  });

  it('should create an instance', () => {
    expect(uiStoreModule).toBeTruthy();
  });
});
