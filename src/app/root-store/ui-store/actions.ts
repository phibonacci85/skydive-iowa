import { Action } from '@ngrx/store';

export enum ActionTypes {
  SET_SIDENAV_OPEN = '[Ui] Set Sidenav Open',
  SET_SIDENAV_CLOSED = '[Ui] Set Sidenav Closed',
  ERROR = '[Ui] Error',
}

export class SetSidenavOpen implements Action {
  readonly type = ActionTypes.SET_SIDENAV_OPEN;
}

export class SetSidenavClosed implements Action {
  readonly type = ActionTypes.SET_SIDENAV_CLOSED;
}

export class Error implements Action {
  readonly type = ActionTypes.ERROR;

  constructor(public payload: any) {}
}

export type Actions = SetSidenavOpen | SetSidenavClosed | Error;
