import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { Queue } from '../../models';
import {
  RootStoreState,
  QueueStoreSelectors,
  QueueStoreActions,
  LoadStoreActions,
  JumpStoreActions,
  JumperStoreActions,
  UsersStoreActions,
} from '../../root-store';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
})
export class ScheduleComponent implements OnInit {

  queues$: Observable<Queue[]>;

  constructor(private store: Store<RootStoreState.State>) { }

  ngOnInit() {
    this.store.dispatch(new UsersStoreActions.LoadRequestAction());
    this.store.dispatch(new JumperStoreActions.LoadRequestAction());
    this.store.dispatch(new JumpStoreActions.LoadRequestAction());
    this.store.dispatch(new LoadStoreActions.LoadRequestAction());
    this.store.dispatch(new QueueStoreActions.LoadRequestAction());
    this.queues$ = this.store.select(QueueStoreSelectors.selectAllQueueItems);
  }
}

