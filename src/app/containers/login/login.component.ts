import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { NgForm } from '@angular/forms';

import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { RootStoreState, AuthStoreSelectors, AuthStoreActions } from '../../root-store';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.theme.scss',
    './login.component.scss',
  ],
})
export class LoginComponent implements OnInit {
  isLoading$: Observable<boolean>;
  error$: Observable<any>;

  constructor(
    private store: Store<RootStoreState.State>,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
    this.isLoading$ = this.store.select(AuthStoreSelectors.selectAuthIsLoading);
    this.error$ = this.store.select(AuthStoreSelectors.selectAuthError);
    this.iconRegistry.addSvgIcon('google',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/btn_google_light_normal_ios.svg'));
    this.iconRegistry.addSvgIcon('facebook',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/facebook_lg.svg'));
    // this.store.dispatch(new AuthStoreActions.Authenticate());
  }

  onSubmit(form: NgForm) {
    this.store.dispatch(new AuthStoreActions.Login({
      username: form.value.email,
      password: form.value.password,
    }));
  }

  googleLogin() {
    this.store.dispatch(new AuthStoreActions.GoogleLogin());
  }

}
