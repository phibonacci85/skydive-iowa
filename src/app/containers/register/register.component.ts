import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { NgForm } from '@angular/forms';

import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { RootStoreState, AuthStoreSelectors, AuthStoreActions } from '../../root-store';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [
    './register-theme.scss',
    './register.component.scss',
  ],
})
export class RegisterComponent implements OnInit {
  isLoading$: Observable<boolean>;
  error$: Observable<any>;

  constructor(
    private store: Store<RootStoreState.State>,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
  ) {
    this.iconRegistry.addSvgIcon('google',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/btn_google_light_normal_ios.svg'));
    this.iconRegistry.addSvgIcon('facebook',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/facebook_lg.svg'));
  }

  ngOnInit() {
    this.isLoading$ = this.store.select(AuthStoreSelectors.selectAuthIsLoading);
    this.error$ = this.store.select(AuthStoreSelectors.selectAuthError);
  }

  onSubmit(form: NgForm) {
    this.store.dispatch(new AuthStoreActions.Register({
      username: form.value.email,
      password: form.value.password,
    }));
  }

  googleRegister() {
    this.store.dispatch(new AuthStoreActions.GoogleRegister());
  }
}
