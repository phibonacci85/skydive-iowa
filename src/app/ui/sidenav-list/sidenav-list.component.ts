import { Store } from '@ngrx/store';
import { Component, Input, OnInit } from '@angular/core';

import { RootStoreState, UiStoreActions, AuthStoreActions } from '../../root-store';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss'],
})
export class SidenavListComponent implements OnInit {
  @Input() authenticated = false;

  constructor(private store: Store<RootStoreState.State>) { }

  ngOnInit() { }

  onCloseSidenav() {
    this.store.dispatch(new UiStoreActions.SetSidenavClosed());
  }

  onLogout() {
    this.store.dispatch(new UiStoreActions.SetSidenavClosed());
    this.store.dispatch(new AuthStoreActions.Logout());
  }
}
