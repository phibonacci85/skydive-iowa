import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { RootStoreState, UiStoreActions, AuthStoreActions } from '../../root-store';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() authenticated = false;

  constructor(
    private store: Store<RootStoreState.State>
  ) { }

  ngOnInit() {}

  onOpenSidenav() {
    this.store.dispatch(new UiStoreActions.SetSidenavOpen());
  }

  onLogout() {
    this.store.dispatch(new AuthStoreActions.Logout());
  }

}
