import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { Queue, Load } from '../../models';
import { RootStoreState, LoadStoreSelectors, LoadStoreActions } from '../../root-store';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.scss']
})
export class QueueComponent implements OnInit {

  @Input() queue: Queue;
  loads$: Observable<Load[]>;

  constructor(private store: Store<RootStoreState.State>) { }

  ngOnInit() {
    this.loads$ = this.store.select(LoadStoreSelectors.selectLoadsByQueueId(this.queue.id));
  }

  addLoad() {
    this.store.dispatch(new LoadStoreActions.CreateLoadAction({queueId: this.queue.id}));
  }
}
