import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { Jumper } from '../../models';
import { RootStoreState, JumperStoreSelectors } from '../../root-store';

@Component({
  selector: 'app-jump',
  templateUrl: './jump.component.html',
  styleUrls: ['./jump.component.scss']
})
export class JumpComponent implements OnInit {

  @Input() jump;
  jumpers$: Observable<Jumper[]>;

  constructor(private store: Store<RootStoreState.State>) { }

  ngOnInit() {
    this.jumpers$ = this.store.select(JumperStoreSelectors.selectJumpersByJumpId(this.jump.id));
  }

}
