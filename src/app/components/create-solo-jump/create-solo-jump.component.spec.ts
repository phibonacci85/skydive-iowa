import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSoloJumpComponent } from './create-solo-jump.component';

describe('CreateSoloJumpComponent', () => {
  let component: CreateSoloJumpComponent;
  let fixture: ComponentFixture<CreateSoloJumpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSoloJumpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSoloJumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
