import { Component, Input, OnInit } from '@angular/core';
import { Jumper, User } from '../../models';
import { Observable } from 'rxjs/Observable';

import { RootStoreState, UsersStoreSelectors } from '../../root-store';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-jumper',
  templateUrl: './jumper.component.html',
  styleUrls: ['./jumper.component.scss'],
})
export class JumperComponent implements OnInit {

  @Input() jumper: Jumper;
  user$: Observable<User>;

  constructor(private store: Store<RootStoreState.State>) { }

  ngOnInit() {
    console.log(this.jumper);
    this.user$ = this.store.select(UsersStoreSelectors.selectUserById(this.jumper.uid));
  }

}
