import { Component, Inject, OnInit } from '@angular/core';
import { Observable } from '../../../../node_modules/rxjs/Observable';
import { User } from '../../models';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Store } from '@ngrx/store';
import * as RootStoreState from '../../root-store/state';
import * as UsersStoreSelectors from '../../root-store/users-store/selectors';
import { NgForm } from '@angular/forms';
import * as JumpStoreActions from '../../root-store/jump-store/actions';

@Component({
  selector: 'app-create-tandem-jump',
  templateUrl: './create-tandem-jump.component.html',
  styleUrls: ['./create-tandem-jump.component.scss']
})
export class CreateTandemJumpComponent implements OnInit {

  jumpers$: Observable<User[]>;

  constructor(
    public dialogRef: MatDialogRef<CreateTandemJumpComponent>,
    @Inject(MAT_DIALOG_DATA) public passedData: any,
    private store: Store<RootStoreState.State>) { }

  ngOnInit() {
    this.jumpers$ = this.store.select(UsersStoreSelectors.selectJumpers(true));
  }

  onSubmit(form: NgForm) {
    this.store.dispatch(new JumpStoreActions.CreateJumpAction({
      type: 'tandem',
      loadId: this.passedData.loadId,
      jumpers: [
        {
          role: 'coach',
          uid: form.value.coach,
        },
        {
          role: 'jumper',
          uid: form.value.jumper,
        },
      ],
    }));
    this.dialogRef.close(true);
  }
}
