import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTandemJumpComponent } from './create-tandem-jump.component';

describe('CreateTandemJumpComponent', () => {
  let component: CreateTandemJumpComponent;
  let fixture: ComponentFixture<CreateTandemJumpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTandemJumpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTandemJumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
