import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateJumpComponent } from './create-jump.component';

describe('CreateJumpComponent', () => {
  let component: CreateJumpComponent;
  let fixture: ComponentFixture<CreateJumpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateJumpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateJumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
