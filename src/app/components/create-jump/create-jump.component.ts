import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { User, Jumper } from '../../models';
import {
  RootStoreState,
  UsersStoreSelectors,
  JumpStoreActions,
} from '../../root-store';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-jump',
  templateUrl: './create-jump.component.html',
  styleUrls: ['./create-jump.component.scss'],
})
export class CreateJumpComponent implements OnInit {

  jumpers$: Observable<User[]>;

  constructor(
    public dialogRef: MatDialogRef<CreateJumpComponent>,
    @Inject(MAT_DIALOG_DATA) public passedData: any,
    private store: Store<RootStoreState.State>) { }

  ngOnInit() {
    this.jumpers$ = this.store.select(UsersStoreSelectors.selectJumpers(true));
  }

  onSubmit(form: NgForm) {
    this.store.dispatch(new JumpStoreActions.CreateJumpAction({
      type: 'solo',
      loadId: this.passedData.loadId,
      jumpers: [
        {
          role: 'jumper',
          uid: form.value.jumper,
        },
      ],
    }));
    this.dialogRef.close(true);
  }
}
