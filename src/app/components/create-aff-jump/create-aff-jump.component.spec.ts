import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAffJumpComponent } from './create-aff-jump.component';

describe('CreateAffJumpComponent', () => {
  let component: CreateAffJumpComponent;
  let fixture: ComponentFixture<CreateAffJumpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAffJumpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAffJumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
