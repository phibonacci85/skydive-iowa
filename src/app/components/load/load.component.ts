import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { Jump, Load } from '../../models';
import {
  RootStoreState,
  JumpStoreSelectors,
  LoadStoreActions,
} from '../../root-store';
import { CreateJumpComponent } from '../create-jump/create-jump.component';
import { MatDialog } from '@angular/material';
import { CreateSoloJumpComponent } from '../create-solo-jump/create-solo-jump.component';
import { CreateAffJumpComponent } from '../create-aff-jump/create-aff-jump.component';
import { CreateTandemJumpComponent } from '../create-tandem-jump/create-tandem-jump.component';

@Component({
  selector: 'app-load',
  templateUrl: './load.component.html',
  styleUrls: ['./load.component.scss'],
})
export class LoadComponent implements OnInit {

  @Input() load: Load;
  jumps$: Observable<Jump[]>;

  constructor(
    private store: Store<RootStoreState.State>, private dialog: MatDialog) { }

  ngOnInit() {
    this.jumps$ = this.store.select(
      JumpStoreSelectors.selectJumpsByLoadId(this.load.id));
  }

  onRemoveLoad() {
    this.store.dispatch(
      new LoadStoreActions.DeleteLoadAction({loadId: this.load.id}));
  }

  onCreateSoloJump() {
    const dialogRef = this.dialog.open(CreateSoloJumpComponent, {
      data: {
        loadId: this.load.id,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

  onCreateAFFJump() {
    const dialogRef = this.dialog.open(CreateAffJumpComponent, {
      data: {
        loadId: this.load.id,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

  onCreateTandemJump() {
    const dialogRef = this.dialog.open(CreateTandemJumpComponent, {
      data: {
        loadId: this.load.id,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }
}
