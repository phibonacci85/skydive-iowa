import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { LoginComponent } from './containers/login/login.component';
import { RegisterComponent } from './containers/register/register.component';
import { HeaderComponent } from './ui/header/header.component';
import { SidenavListComponent } from './ui/sidenav-list/sidenav-list.component';
import { WelcomeComponent } from './containers/welcome/welcome.component';
import { ProfileComponent } from './profile/profile.component';
import { PageNotFoundComponent } from './containers/page-not-found/page-not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { BasicInfoComponent } from './profile/basic-info/basic-info.component';
import { AuthGuard } from './guards';
import { RootStoreModule } from './root-store';
import { QueueComponent } from './components/queue/queue.component';
import { LoadComponent } from './components/load/load.component';
import { JumpComponent } from './components/jump/jump.component';
import { ScheduleComponent } from './containers/schedule/schedule.component';
import { JumperComponent } from './components/jumper/jumper.component';
import { CreateJumpComponent } from './components/create-jump/create-jump.component';
import { CreateTandemJumpComponent } from './components/create-tandem-jump/create-tandem-jump.component';
import { CreateAffJumpComponent } from './components/create-aff-jump/create-aff-jump.component';
import { CreateSoloJumpComponent } from './components/create-solo-jump/create-solo-jump.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    SidenavListComponent,
    WelcomeComponent,
    ProfileComponent,
    PageNotFoundComponent,
    BasicInfoComponent,
    ScheduleComponent,
    QueueComponent,
    LoadComponent,
    JumpComponent,
    JumperComponent,
    CreateJumpComponent,
    CreateTandemJumpComponent,
    CreateAffJumpComponent,
    CreateSoloJumpComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    HttpClientModule,
    RootStoreModule,
  ],
  entryComponents: [
    CreateSoloJumpComponent, CreateTandemJumpComponent, CreateAffJumpComponent
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
