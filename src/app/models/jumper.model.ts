export interface Jumper {
  id?: string;
  role: string;
  uid: string;
  jumpId: string;
}
