export interface Jump {
  id: string;
  type: string;
  loadId: string;
}
