import { Aircraft } from './aircraft.model';

export interface Queue {
  id: string;
  aircraft: Aircraft;
}
