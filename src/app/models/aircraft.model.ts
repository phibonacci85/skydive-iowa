export interface Aircraft {
  displayName: string;
  tailNumber: string;
  make: string;
  model: string;
}