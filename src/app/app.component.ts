import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { User } from './models';

import {
  RootStoreState,
  RootStoreSelectors,
  AuthStoreSelectors,
  AuthStoreActions,
  UiStoreSelectors,
  UiStoreActions,
} from './root-store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  user$: Observable<User>;
  loading$: Observable<boolean>;
  error$: Observable<any>;
  showSidenav$: Observable<boolean>;
  mode = 'push';

  constructor(private store: Store<RootStoreState.State>) {}

  ngOnInit() {
    this.user$ = this.store.select(AuthStoreSelectors.selectAuthUser);
    this.loading$ = this.store.select(RootStoreSelectors.selectIsLoading);
    this.error$ = this.store.select(RootStoreSelectors.selectError);
    this.showSidenav$ = this.store.select(UiStoreSelectors.selectUiIsSidenavOpen);
    this.store.dispatch(new AuthStoreActions.Authenticate());
  }

  onCloseSidenav() {
    this.store.dispatch(new UiStoreActions.SetSidenavClosed());
  }
}
